﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class dataModelContainer : DbContext
    {
        public dataModelContainer()
            : base("name=dataModelContainer")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<HolidayList> HolidayLists { get; set; }
        public virtual DbSet<Holiday> Holidays { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<Market> Markets { get; set; }
        public virtual DbSet<Index> Indices { get; set; }
        public virtual DbSet<IndexPrice> IndexPrices { get; set; }
        public virtual DbSet<Instrument> Instruments { get; set; }
        public virtual DbSet<InstPrice> InstPrices { get; set; }
        public virtual DbSet<Exchange> Exchanges { get; set; }
        public virtual DbSet<PriceSource> PriceSources { get; set; }
        public virtual DbSet<InstAlias> InstAlias { get; set; }
        public virtual DbSet<FutMarketAlias> FutMarketAlias { get; set; }
        public virtual DbSet<UnderMap> UnderMaps { get; set; }
        public virtual DbSet<MapInstruction> MapInstructions { get; set; }
        public virtual DbSet<Trade> Trades { get; set; }
        public virtual DbSet<TradeFee> TradeFees { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Counterparty> Counterparties { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Deal> Deals { get; set; }
        public virtual DbSet<DealOrder> DealOrders { get; set; }
        public virtual DbSet<Chain> Chains { get; set; }
        public virtual DbSet<ChainSection> ChainSections { get; set; }
        public virtual DbSet<SectionPoint> SectionPoints { get; set; }
    }
}

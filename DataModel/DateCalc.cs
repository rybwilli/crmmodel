﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public partial class HolidayList
    {
        private Dictionary<string, int> holiCountCache = new Dictionary<string, int>();
        private Dictionary<string, int> dayCountCache = new Dictionary<string, int>();
        public double bizYearFraction(DateTime start,DateTime end)
        {
            return OneBizDayYearFraction() * (double)bizDaysBetween(start, end);
        }
        public double OneBizDayYearFraction()
        {
            return 1.0 / (double)DaysPerYear;
        }
        public int bizDaysBetween(DateTime dtmStart, DateTime dtmEnd)
        {
            // return 0 if end <= start
            int sgn = 1;
            DateTime start = dtmStart;
            DateTime end = dtmEnd;
            if (dtmEnd <= dtmStart)
            {
                sgn = -1;
                end = dtmStart;
                start = dtmEnd;
            }
            string key = start.ToShortDateString() + end.ToShortDateString();
            int retOut = 0;
            if (!dayCountCache.TryGetValue(key, out retOut))
            {
                removeFirstDayElements();
                // set end back to most recent bizday
                end = makeSureItsABizDay(end, false);
                // set start forward to next bizday
                start = makeSureItsABizDay(start, true);
                // get
                int dowStart = (int)start.DayOfWeek;
                int dowEnd = (int)end.DayOfWeek;
                TimeSpan tSpan = end - start;
                // compute weekdays between
                // since
                int weekDays = (dowStart <= dowEnd) ?
                    (((tSpan.Days / 7) * 5) + Math.Max((Math.Min((dowEnd + 1), 6) - dowStart), 0)) - 1 :
                    (((tSpan.Days / 7) * 5) + Math.Min((dowEnd + 6) - Math.Min(dowStart, 6), 5)) - 1;
                retOut = weekDays - numberHoliday(start, end);
                dayCountCache.Add(key, retOut);
            }
            return retOut * sgn;
        }
        public DateTime addDays(DateTime start, int dayDelta)
        {
            int i = 0;
            int sgn = Math.Sign(dayDelta);
            while (i != dayDelta)
            {
                do
                {
                    start = start.AddDays(sgn);
                }
                while (isHolidayOrWeekend(start));
                i = i + sgn;
            }
            return start;
        }
        public DateTime firstBizDayOfMonth(DateTime dt)
        {
            dt = dt.AddDays(1 - dt.Day);
            while (isHolidayOrWeekend(dt))
            {
                dt = dt.AddDays(1);
            }
            return dt;
        }
        public DateTime lastBizDayOfMonth(DateTime dt)
        {
            dt = new DateTime(dt.Year, dt.Month, 1).AddMonths(1).AddDays(-1);
            while (isHolidayOrWeekend(dt))
            {
                dt = dt.AddDays(-1);
            }
            return dt;
        }
        public bool isHolidayOrWeekend(DateTime d)
        {
            return numberHoliday(d, d) > 0 || d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday;
        }
        public DateTime makeSureItsABizDay(DateTime d, bool isForward)
        {
            while (isHolidayOrWeekend(d)) d = d.AddDays(isForward ? 1 : -1);
            return d;
        }

        // this method calculates the nuber of holidays between to days for a given market
        // it stores the result in a cache so as not to perform the same calculation many times
        // this looks really bad right now. TODO make this tighter/better
        public int numberHoliday(DateTime fromDT, DateTime toDT)
        {
            string key = fromDT.ToShortDateString() + toDT.ToShortDateString();
            int retOut = 0;
            if (!holiCountCache.TryGetValue(key, out retOut))
            {
                removeFirstHoliElements();
                retOut = this.Holidays.Where(x => x.Date >= fromDT && x.Date <= toDT).Count();
                holiCountCache.Add(key, retOut);
            }
            return retOut;
        }
        public void removeFirstHoliElements()
        {
            // if cache gets too big (over 10000)
            // then remove first 250 elements
            if (holiCountCache.Count > 10000)
            {
                for (int i = 0; i < 250; i++)
                    holiCountCache.Remove(holiCountCache.ElementAt(i).Key);
            }
        }
        public void removeFirstDayElements()
        {
            // if cache gets too big (over 10000)
            // then remove first 250 elements
            if (dayCountCache.Count > 10000)
            {
                for (int i = 0; i < 250; i++)
                    dayCountCache.Remove(dayCountCache.ElementAt(i).Key);
            }
        }
    }
}

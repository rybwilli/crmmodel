﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class MarketState
    {
        public DateTime valueDate { get; set; }
        private dataModelContainer _dmc;
        public dataModelContainer dmc { get { return _dmc; } set { _dmc = value; } }
        private List<UnderRecord> urecs = new List<UnderRecord>();
        private List<DerivRecord> drecs = new List<DerivRecord>();
        public MarketState(dataModelContainer _data, DateTime _vdate)
        {
            _dmc = _data;
            valueDate = _vdate;
        }
        public void initMarketNoVol(string symbol)
        {
            var market = dmc.Markets.OfType<FuturesMarket>().SingleOrDefault(x => x.Symbol == symbol.ToUpper());
            var unders = market.Futures.Where(x => x.Expiration >= valueDate);
            foreach (var u in unders)
            {
                var upx = initUnderRecord(u.Symbol);
                if (upx != null) upx.updateSettle();
            }
        }
        public UnderRecord evaluateUnderRecord(string symbol)
        {
            var upx = (UnderRecord)getUnderRecord(symbol);
            
            // here is the place we'd check a few sources and update best bid and offer
            // TODO link up real time/broker sources
            var fmkt = (FuturesMarket)upx.under.getMarket();
            upx.bid = upx.settle - fmkt.MinTick;
            upx.bidsource = "Settle";
            upx.ask = upx.settle + fmkt.MinTick;
            upx.asksource = "Settle";
            upx.fair = upx.settle;
            upx = upx.simpleCopy();
            // then check implied prices from UnderMaps
            foreach(var m in upx.under.UnderMaps.Where(x => x.isActive))
            {
                double? impbid = m.PriceAddition;
                double? impask = m.PriceAddition;
                Dictionary<double,UnderRecord> recs = new Dictionary<double,UnderRecord>();
                foreach(var instruct in m.MapInstructions)
                {
                    var ipx = evaluateUnderRecord(instruct.SourceUnderlying.Symbol);
                    recs.Add(instruct.QuantityRatio, ipx);
                    if (!instruct.mult.HasValue) instruct.initMult();
                    impbid += instruct.mult.Value * ((instruct.mult.Value > 0 ? ipx.bid : ipx.ask) + instruct.Addition - instruct.Buffer);
                    impask += instruct.mult.Value * ((instruct.mult.Value > 0 ? ipx.ask : ipx.bid) + instruct.Addition + instruct.Buffer);
                }
                if (impbid>upx.bid)
                {
                    upx.bid = impbid;

                    foreach(var r in recs)
                    {
                        if (r.Value.bidHedges.Count()>0)
                        {

                        }
                        else
                        {
                            upx.bidHedges.Clear();
                            UnderHedge h = new UnderHedge {undersymbol = r.Value.under.Symbol,price = r.Key>0 ? r.Value.bid.Value : r.Value.ask.Value,ratio = r.Key, quantity = r.Key };
                            upx.bidHedges.Add(h);
                        }

                    }
                }
                if (impask<upx.ask)
                {
                    upx.ask = impask;
                    foreach(var r in recs)
                    {
                        if (r.Value.askHedges.Count() > 0)
                        {

                        }
                        else
                        {
                            upx.askHedges.Clear();
                            UnderHedge h = new UnderHedge { undersymbol = r.Value.under.Symbol, price = r.Key > 0 ? r.Value.ask.Value : r.Value.bid.Value, ratio = r.Key, quantity = r.Key };
                            upx.askHedges.Add(h);
                        }
                    }
                }
            }
            return upx;
        }
        public MarketState createSnapshotNoVol(IEnumerable<Underlying> unders)
        {
            var ms = new MarketState(this.dmc, this.valueDate);
            foreach(var u in unders)
            {
                var upx = this.getUnderRecord(u.Symbol);
            }
            return ms;
        }
        public UnderRecord initUnderRecord(string symbol)
        {
            UnderRecord upx = urecs.SingleOrDefault(x => x.under.Symbol == symbol);
            if (upx==null)
            {
                Underlying u = dmc.Instruments.Include("UnderMaps").OfType<Underlying>().SingleOrDefault(x => x.Symbol == symbol);
                if (u!=null)
                {
                    u.initMap();
                    if (u.PrimaryMap != null)
                    {
                        foreach (var mi in u.PrimaryMap.MapInstructions)
                        {
                            var mirec = initUnderRecord(mi.SourceUnderlying.Symbol);
                        }
                    }
                    UnderRecord ur = new UnderRecord(u) { valueDate = this.valueDate };

                    urecs.Add(ur);
                    upx = ur;
                }
            }
            return upx;
        }
        public UnderRecord getUnderRecord(string symbol)
        {
            return urecs.SingleOrDefault(x => x.under.Symbol == symbol.ToUpper());
        }
        public void removeUnderRecord(UnderRecord upr)
        {
            urecs.Remove(upr);
        }
        public DerivRecord initDerivRecord(string symbol)
        {
            DerivRecord dpx = drecs.SingleOrDefault(x => x.deriv.Symbol == symbol);
            if (dpx == null)
            {
                Derivative d = dmc.Instruments.Include("Underlying").OfType<Derivative>().SingleOrDefault(x => x.Symbol == symbol);
                if (d != null)
                {
                    var upx = initUnderRecord(d.Underlying.Symbol);
                    DerivRecord dr = new DerivRecord(d) { valueDate = this.valueDate };
                    drecs.Add(dr);
                    dpx = dr;
                }
            }
            return dpx;
        }
        public DerivRecord getDerivRecord(string symbol)
        {
            return drecs.SingleOrDefault(x => x.deriv.Symbol == symbol.ToUpper());
        }
        public void removeDerivRecord(DerivRecord dpr)
        {
            drecs.Remove(dpr);
        }
    }
    public class PriceRecord
    {
        public DateTime valueDate { get; set; }
        public double dayfrac { get; set; }
        public DateTime? settleDate { get; set; }
        public double? settle { get; set; }
        public double? bid { get; set; }
        public double? ask { get; set; }
        public double? fair { get; set; }
        public double? mid { get { if (bid.HasValue && ask.HasValue) return 0.5 * (bid.Value + ask.Value); else return  null; } private set { } }
        public void updateSettle(Instrument i)
        {
            var x = i.InstPrices.OrderByDescending(y => y.Date).LastOrDefault();
            if (x != null)
            {
                settle = x.Price;
                settleDate = x.Date;
            }
            else
            {
                settle = null;
                settleDate = null;
            }
        }
    }
    public class UnderRecord : PriceRecord
    {
        public Underlying under;
        public string bidsource { get; set; }
        public string asksource { get; set; }
        public List<UnderHedge> bidHedges = new List<UnderHedge>();
        public List<UnderHedge> askHedges = new List<UnderHedge>();
        public UnderRecord(Underlying _u)
        {
            under = _u;        
        }
        public void updateSettle()
        {
            updateSettle(under);
        }
        public UnderRecord simpleCopy()
        {
            // we don't want the underhedges to follow here
            return new UnderRecord(this.under)
            {
                bid = this.bid,
                ask = this.ask,
                settle = this.settle,
                settleDate = this.settleDate,
                dayfrac = this.dayfrac,
                valueDate = this.valueDate,
                fair = this.fair,
                bidsource = this.bidsource,
                asksource = this.asksource
            };
        }
        //public UnderRecord createSnap()
        //{
        //    var ur = (UnderRecord)this.MemberwiseClone();
        //    if (ur.under.PrimaryMap!=null)
        //    {

        //    }
        //}
    }
    public class UnderHedge
    {
        public double quantity { get; set; }
        public double ratio { get; set; }
        public double price { get; set; }
        public string undersymbol { get; set; }
        public string source { get; set; }
    }
    public partial class MapInstruction
    {
        public double? mult { get; set; }
        public void initMult()
        {
            mult = this.QuantityRatio * this.UnderMap.Underlying.quantityMulitplier() / this.SourceUnderlying.quantityMulitplier();
        }
    }
    public class DerivRecord : PriceRecord
    {
        public Derivative deriv;
        public DerivRecord(Derivative _d)
        {
            deriv = _d;
        }
        public void updateSettle()
        {
            updateSettle(deriv);
        }
    }

    public class Exposure
    {
        public UnderRecord urec;
        public double delta;
        public double gamma;
        public double vega;
        public double theta;
        public double rho;

    }
}

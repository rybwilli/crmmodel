﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public partial class InterestRateMarket
    {
        public Dictionary<DateTime, InterestRateCurve> curves = new Dictionary<DateTime, InterestRateCurve>();
        public double getDiscountFactor(DateTime fromDate, DateTime toDate)
        {
            if (!curves.ContainsKey(fromDate.Date))
            {
                populateCurve(fromDate.Date);
            }
            // no rates means no discounting. maybe have a warning
            if (!curves.ContainsKey(fromDate.Date)) return 1;
            else return curves[fromDate.Date].getDiscountFactor(toDate);
        }
        private void populateCurve(DateTime dt)
        {
            // we'll figure out what to do with non exact dates
            int ctr = 0;
            var rates = this.InterestRates.Where(x => x.InstPrices.SingleOrDefault(y => y.Date == dt.AddDays(ctr)) != null).OrderBy(x => x.Tenor);
            while (rates.Count() == 0 || ctr == -14)
            {
                ctr--;
                rates = this.InterestRates.Where(x => x.InstPrices.SingleOrDefault(y => y.Date == dt.AddDays(ctr)) != null).OrderBy(x => x.Tenor);
            }
            if (rates.Count() > 0)
            {
                InterestRateCurve irc = new InterestRateCurve()
                {
                    requestDate = dt,
                    actualDate = dt.AddDays(ctr)
                };
                foreach (var r in rates)
                {
                    switch (Convention)
                    {
                        // let's do an actual 365 calculation to start for everything to get us in the right ballpark
                        default:
                            InstPrice ip = r.InstPrices.Where(x => x.Date <= dt).OrderByDescending(x => x.Date).First();
                            DateTime edt = dt;
                            // assume monthly tenor convention right now
                            if (r.Tenor > 0)
                            {
                                if (r.Tenor < 1)
                                {
                                    edt = dt.AddDays(7 * r.Tenor * 4);
                                }
                                else
                                {
                                    edt = dt.AddMonths((int)r.Tenor);
                                }
                            }
                            double ccrate = r.Tenor > 0 ? 365.0 / (double)(edt - dt).Days * Math.Log((1.0 + (double)ip.Rate * (double)(edt - dt).Days / 365.0)) : (double)ip.Rate;
                            irc.rates.Add((edt - dt).Days, ccrate * 0.01);
                            break;
                    }
                }
                curves.Add(dt, irc);
            }
            // not sure what to do without rate data right now

        }
    }
    // ideally this is calendar days and continuously compounded rates
    public class InterestRateCurve
    {
        public DateTime requestDate;
        public DateTime actualDate;
        public Dictionary<int, double> rates = new Dictionary<int, double>();

        private Dictionary<int, double> discFacCache = new Dictionary<int, double>();
        public double getDiscountFactor(DateTime toDt)
        {
            int days = (toDt - requestDate).Days;
            if (discFacCache.ContainsKey(days)) return discFacCache[days];
            double dfac = 1;
            if (days <= rates.ElementAt(0).Key) dfac = Math.Exp(-rates.ElementAt(0).Value * (double)days / 365.0);
            else if (days >= rates.ElementAt(rates.Count - 1).Key) dfac = Math.Exp(-rates.ElementAt(rates.Count - 1).Value * (double)days / 365.0);
            else
            {
                int ctr = 0;
                foreach (KeyValuePair<int, double> pair in rates)
                {
                    if (pair.Key >= days) break;
                    ctr++;
                }
                KeyValuePair<int, double> pair0 = rates.ElementAt(ctr - 1);
                KeyValuePair<int, double> pair1 = rates.ElementAt(ctr);
                double ccrate = (pair0.Value * (pair1.Key - days) + pair1.Value * (days - pair0.Key)) / (pair1.Key - pair0.Key);
                dfac = Math.Exp(-rates.ElementAt(rates.Count - 1).Value * (double)days / 365.0);
            }
            discFacCache.Add(days, dfac);
            return dfac;
        }
    }
}

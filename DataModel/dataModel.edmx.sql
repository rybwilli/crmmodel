
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/17/2017 13:58:28
-- Generated from EDMX file: C:\Users\Chris Prouty\Documents\crmsrc\crmmodel\DataModel\dataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [CRMProd];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_HolidayListHoliday]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Holidays] DROP CONSTRAINT [FK_HolidayListHoliday];
GO
IF OBJECT_ID(N'[dbo].[FK_HolidayListCurrency]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Currencies] DROP CONSTRAINT [FK_HolidayListCurrency];
GO
IF OBJECT_ID(N'[dbo].[FK_HolidayListMarket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets] DROP CONSTRAINT [FK_HolidayListMarket];
GO
IF OBJECT_ID(N'[dbo].[FK_CurrencyMarket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets] DROP CONSTRAINT [FK_CurrencyMarket];
GO
IF OBJECT_ID(N'[dbo].[FK_HolidayListIndex]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Indices] DROP CONSTRAINT [FK_HolidayListIndex];
GO
IF OBJECT_ID(N'[dbo].[FK_MarketIndex]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Indices] DROP CONSTRAINT [FK_MarketIndex];
GO
IF OBJECT_ID(N'[dbo].[FK_IndexIndexPrice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[IndexPrices] DROP CONSTRAINT [FK_IndexIndexPrice];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentInstPrice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InstPrices] DROP CONSTRAINT [FK_InstrumentInstPrice];
GO
IF OBJECT_ID(N'[dbo].[FK_CurrencyForwardFXMarket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets_ForwardFXMarket] DROP CONSTRAINT [FK_CurrencyForwardFXMarket];
GO
IF OBJECT_ID(N'[dbo].[FK_UnderlyingDerivative]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Derivative] DROP CONSTRAINT [FK_UnderlyingDerivative];
GO
IF OBJECT_ID(N'[dbo].[FK_FuturesMarketFuture]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Future] DROP CONSTRAINT [FK_FuturesMarketFuture];
GO
IF OBJECT_ID(N'[dbo].[FK_ExchangeInstrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments] DROP CONSTRAINT [FK_ExchangeInstrument];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentInstAlias]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InstAlias] DROP CONSTRAINT [FK_InstrumentInstAlias];
GO
IF OBJECT_ID(N'[dbo].[FK_PriceSourceInstAlias]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[InstAlias] DROP CONSTRAINT [FK_PriceSourceInstAlias];
GO
IF OBJECT_ID(N'[dbo].[FK_PriceSourceFutMarketAlias]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FutMarketAlias] DROP CONSTRAINT [FK_PriceSourceFutMarketAlias];
GO
IF OBJECT_ID(N'[dbo].[FK_FuturesMarketFutMarketAlias]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FutMarketAlias] DROP CONSTRAINT [FK_FuturesMarketFutMarketAlias];
GO
IF OBJECT_ID(N'[dbo].[FK_UnderlyingUnderMap]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UnderMaps] DROP CONSTRAINT [FK_UnderlyingUnderMap];
GO
IF OBJECT_ID(N'[dbo].[FK_UnderMapMapInstruction]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MapInstructions] DROP CONSTRAINT [FK_UnderMapMapInstruction];
GO
IF OBJECT_ID(N'[dbo].[FK_UnderlyingMapInstruction]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MapInstructions] DROP CONSTRAINT [FK_UnderlyingMapInstruction];
GO
IF OBJECT_ID(N'[dbo].[FK_InterestRateMarketInterestRate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_InterestRate] DROP CONSTRAINT [FK_InterestRateMarketInterestRate];
GO
IF OBJECT_ID(N'[dbo].[FK_InterestRateMarketZeroCouponBond]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_ZeroCouponBond] DROP CONSTRAINT [FK_InterestRateMarketZeroCouponBond];
GO
IF OBJECT_ID(N'[dbo].[FK_ForwardFXMarketForwardFXRate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_ForwardFXRate] DROP CONSTRAINT [FK_ForwardFXMarketForwardFXRate];
GO
IF OBJECT_ID(N'[dbo].[FK_ForwardFXMarketTenorFXRate]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_TenorFXRate] DROP CONSTRAINT [FK_ForwardFXMarketTenorFXRate];
GO
IF OBJECT_ID(N'[dbo].[FK_InstrumentTrade]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trades] DROP CONSTRAINT [FK_InstrumentTrade];
GO
IF OBJECT_ID(N'[dbo].[FK_TradeTradeFee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TradeFees] DROP CONSTRAINT [FK_TradeTradeFee];
GO
IF OBJECT_ID(N'[dbo].[FK_CurrencyTradeFee]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TradeFees] DROP CONSTRAINT [FK_CurrencyTradeFee];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerCounterparty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Counterparties] DROP CONSTRAINT [FK_CustomerCounterparty];
GO
IF OBJECT_ID(N'[dbo].[FK_CounterpartyTrade]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trades] DROP CONSTRAINT [FK_CounterpartyTrade];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerUser_Customer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerUser] DROP CONSTRAINT [FK_CustomerUser_Customer];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerUser_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerUser] DROP CONSTRAINT [FK_CustomerUser_User];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerDeal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Deals] DROP CONSTRAINT [FK_CustomerDeal];
GO
IF OBJECT_ID(N'[dbo].[FK_DealDealOrder]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DealOrders] DROP CONSTRAINT [FK_DealDealOrder];
GO
IF OBJECT_ID(N'[dbo].[FK_UnderlyingChain]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Chains] DROP CONSTRAINT [FK_UnderlyingChain];
GO
IF OBJECT_ID(N'[dbo].[FK_ChainChainSection]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChainSections] DROP CONSTRAINT [FK_ChainChainSection];
GO
IF OBJECT_ID(N'[dbo].[FK_ChainSectionSectionPoint]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SectionPoints] DROP CONSTRAINT [FK_ChainSectionSectionPoint];
GO
IF OBJECT_ID(N'[dbo].[FK_ChainOption]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Option] DROP CONSTRAINT [FK_ChainOption];
GO
IF OBJECT_ID(N'[dbo].[FK_ForwardFXMarket_inherits_Market]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets_ForwardFXMarket] DROP CONSTRAINT [FK_ForwardFXMarket_inherits_Market];
GO
IF OBJECT_ID(N'[dbo].[FK_Underlying_inherits_Instrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Underlying] DROP CONSTRAINT [FK_Underlying_inherits_Instrument];
GO
IF OBJECT_ID(N'[dbo].[FK_Derivative_inherits_Instrument]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Derivative] DROP CONSTRAINT [FK_Derivative_inherits_Instrument];
GO
IF OBJECT_ID(N'[dbo].[FK_FuturesMarket_inherits_Market]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets_FuturesMarket] DROP CONSTRAINT [FK_FuturesMarket_inherits_Market];
GO
IF OBJECT_ID(N'[dbo].[FK_Future_inherits_Underlying]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Future] DROP CONSTRAINT [FK_Future_inherits_Underlying];
GO
IF OBJECT_ID(N'[dbo].[FK_InterestRateMarket_inherits_Market]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Markets_InterestRateMarket] DROP CONSTRAINT [FK_InterestRateMarket_inherits_Market];
GO
IF OBJECT_ID(N'[dbo].[FK_InterestRate_inherits_Underlying]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_InterestRate] DROP CONSTRAINT [FK_InterestRate_inherits_Underlying];
GO
IF OBJECT_ID(N'[dbo].[FK_ZeroCouponBond_inherits_Underlying]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_ZeroCouponBond] DROP CONSTRAINT [FK_ZeroCouponBond_inherits_Underlying];
GO
IF OBJECT_ID(N'[dbo].[FK_ForwardFXRate_inherits_Underlying]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_ForwardFXRate] DROP CONSTRAINT [FK_ForwardFXRate_inherits_Underlying];
GO
IF OBJECT_ID(N'[dbo].[FK_TenorFXRate_inherits_Underlying]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_TenorFXRate] DROP CONSTRAINT [FK_TenorFXRate_inherits_Underlying];
GO
IF OBJECT_ID(N'[dbo].[FK_Option_inherits_Derivative]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Option] DROP CONSTRAINT [FK_Option_inherits_Derivative];
GO
IF OBJECT_ID(N'[dbo].[FK_Swap_inherits_Derivative]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Swap] DROP CONSTRAINT [FK_Swap_inherits_Derivative];
GO
IF OBJECT_ID(N'[dbo].[FK_IndexFuture_inherits_Future]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_IndexFuture] DROP CONSTRAINT [FK_IndexFuture_inherits_Future];
GO
IF OBJECT_ID(N'[dbo].[FK_European_inherits_Option]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_European] DROP CONSTRAINT [FK_European_inherits_Option];
GO
IF OBJECT_ID(N'[dbo].[FK_American_inherits_Option]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_American] DROP CONSTRAINT [FK_American_inherits_Option];
GO
IF OBJECT_ID(N'[dbo].[FK_Digital_inherits_Option]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Digital] DROP CONSTRAINT [FK_Digital_inherits_Option];
GO
IF OBJECT_ID(N'[dbo].[FK_Asian_inherits_Option]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Asian] DROP CONSTRAINT [FK_Asian_inherits_Option];
GO
IF OBJECT_ID(N'[dbo].[FK_DigitalAsian_inherits_Asian]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_DigitalAsian] DROP CONSTRAINT [FK_DigitalAsian_inherits_Asian];
GO
IF OBJECT_ID(N'[dbo].[FK_Barrier_inherits_Option]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Instruments_Barrier] DROP CONSTRAINT [FK_Barrier_inherits_Option];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[HolidayLists]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HolidayLists];
GO
IF OBJECT_ID(N'[dbo].[Holidays]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Holidays];
GO
IF OBJECT_ID(N'[dbo].[Currencies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Currencies];
GO
IF OBJECT_ID(N'[dbo].[Markets]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Markets];
GO
IF OBJECT_ID(N'[dbo].[Indices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Indices];
GO
IF OBJECT_ID(N'[dbo].[IndexPrices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[IndexPrices];
GO
IF OBJECT_ID(N'[dbo].[Instruments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments];
GO
IF OBJECT_ID(N'[dbo].[InstPrices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstPrices];
GO
IF OBJECT_ID(N'[dbo].[Exchanges]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Exchanges];
GO
IF OBJECT_ID(N'[dbo].[PriceSources]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PriceSources];
GO
IF OBJECT_ID(N'[dbo].[InstAlias]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstAlias];
GO
IF OBJECT_ID(N'[dbo].[FutMarketAlias]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FutMarketAlias];
GO
IF OBJECT_ID(N'[dbo].[UnderMaps]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UnderMaps];
GO
IF OBJECT_ID(N'[dbo].[MapInstructions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MapInstructions];
GO
IF OBJECT_ID(N'[dbo].[Trades]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trades];
GO
IF OBJECT_ID(N'[dbo].[TradeFees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TradeFees];
GO
IF OBJECT_ID(N'[dbo].[Customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customers];
GO
IF OBJECT_ID(N'[dbo].[Counterparties]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Counterparties];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Deals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Deals];
GO
IF OBJECT_ID(N'[dbo].[DealOrders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DealOrders];
GO
IF OBJECT_ID(N'[dbo].[Chains]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Chains];
GO
IF OBJECT_ID(N'[dbo].[ChainSections]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChainSections];
GO
IF OBJECT_ID(N'[dbo].[SectionPoints]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SectionPoints];
GO
IF OBJECT_ID(N'[dbo].[Markets_ForwardFXMarket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Markets_ForwardFXMarket];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Underlying]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Underlying];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Derivative]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Derivative];
GO
IF OBJECT_ID(N'[dbo].[Markets_FuturesMarket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Markets_FuturesMarket];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Future]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Future];
GO
IF OBJECT_ID(N'[dbo].[Markets_InterestRateMarket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Markets_InterestRateMarket];
GO
IF OBJECT_ID(N'[dbo].[Instruments_InterestRate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_InterestRate];
GO
IF OBJECT_ID(N'[dbo].[Instruments_ZeroCouponBond]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_ZeroCouponBond];
GO
IF OBJECT_ID(N'[dbo].[Instruments_ForwardFXRate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_ForwardFXRate];
GO
IF OBJECT_ID(N'[dbo].[Instruments_TenorFXRate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_TenorFXRate];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Option]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Option];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Swap]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Swap];
GO
IF OBJECT_ID(N'[dbo].[Instruments_IndexFuture]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_IndexFuture];
GO
IF OBJECT_ID(N'[dbo].[Instruments_European]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_European];
GO
IF OBJECT_ID(N'[dbo].[Instruments_American]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_American];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Digital]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Digital];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Asian]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Asian];
GO
IF OBJECT_ID(N'[dbo].[Instruments_DigitalAsian]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_DigitalAsian];
GO
IF OBJECT_ID(N'[dbo].[Instruments_Barrier]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Instruments_Barrier];
GO
IF OBJECT_ID(N'[dbo].[CustomerUser]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomerUser];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'HolidayLists'
CREATE TABLE [dbo].[HolidayLists] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(256)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [DaysPerYear] int  NOT NULL
);
GO

-- Creating table 'Holidays'
CREATE TABLE [dbo].[Holidays] (
    [Date] datetime  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [HolidayListId] int  NOT NULL
);
GO

-- Creating table 'Currencies'
CREATE TABLE [dbo].[Currencies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Symbol] nvarchar(32)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [HolidayListId] int  NOT NULL
);
GO

-- Creating table 'Markets'
CREATE TABLE [dbo].[Markets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Symbol] nvarchar(32)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [URL] nvarchar(max)  NULL,
    [Group] nvarchar(64)  NOT NULL,
    [SubGroup] nvarchar(64)  NOT NULL,
    [HolidayListId] int  NOT NULL,
    [CurrencyId] int  NOT NULL
);
GO

-- Creating table 'Indices'
CREATE TABLE [dbo].[Indices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Symbol] nvarchar(32)  NOT NULL,
    [Units] nvarchar(32)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [CurrencyMult] int  NOT NULL,
    [HolidayListId] int  NOT NULL,
    [Market_Id] int  NULL
);
GO

-- Creating table 'IndexPrices'
CREATE TABLE [dbo].[IndexPrices] (
    [Date] datetime  NOT NULL,
    [IndexId] int  NOT NULL,
    [PriceType] int  NOT NULL,
    [Price] float  NOT NULL
);
GO

-- Creating table 'Instruments'
CREATE TABLE [dbo].[Instruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Symbol] nvarchar(128)  NOT NULL,
    [Expiration] datetime  NOT NULL,
    [SettleDate] datetime  NOT NULL,
    [Status] int  NOT NULL,
    [SettleType] int  NOT NULL,
    [ExchangeId] int  NULL
);
GO

-- Creating table 'InstPrices'
CREATE TABLE [dbo].[InstPrices] (
    [Date] datetime  NOT NULL,
    [Price] float  NOT NULL,
    [Rate] float  NULL,
    [PriceType] int  NOT NULL,
    [DiscFactor] float  NULL,
    [InstrumentId] int  NOT NULL
);
GO

-- Creating table 'Exchanges'
CREATE TABLE [dbo].[Exchanges] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(32)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PriceSources'
CREATE TABLE [dbo].[PriceSources] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(32)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InstAlias'
CREATE TABLE [dbo].[InstAlias] (
    [Symbol] nvarchar(128)  NOT NULL,
    [PriceMult] int  NOT NULL,
    [InstrumentId] int  NOT NULL,
    [PriceSourceId] int  NOT NULL
);
GO

-- Creating table 'FutMarketAlias'
CREATE TABLE [dbo].[FutMarketAlias] (
    [SymbolPrefix] nvarchar(128)  NOT NULL,
    [PriceMult] int  NOT NULL,
    [PriceSourceId] int  NOT NULL,
    [FuturesMarketId] int  NOT NULL
);
GO

-- Creating table 'UnderMaps'
CREATE TABLE [dbo].[UnderMaps] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] int  NOT NULL,
    [Group] nvarchar(64)  NOT NULL,
    [isActive] bit  NOT NULL,
    [PriceAddition] float  NOT NULL,
    [UnderlyingId] int  NOT NULL
);
GO

-- Creating table 'MapInstructions'
CREATE TABLE [dbo].[MapInstructions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [QuantityRatio] float  NOT NULL,
    [Addition] float  NOT NULL,
    [Buffer] float  NOT NULL,
    [ValidDate] datetime  NOT NULL,
    [UnderMapId] int  NOT NULL,
    [SourceUnderlyingId] int  NOT NULL
);
GO

-- Creating table 'Trades'
CREATE TABLE [dbo].[Trades] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [InstrumentId] int  NOT NULL,
    [Quantity] float  NOT NULL,
    [Price] float  NOT NULL,
    [RiskPrice] float  NOT NULL,
    [Status] int  NOT NULL,
    [ExecutionDate] nvarchar(max)  NOT NULL,
    [ExecutionTime] nvarchar(max)  NOT NULL,
    [ConfirmDate] datetime  NULL,
    [InvoiceDate] datetime  NULL,
    [PremiumPaidDate] datetime  NULL,
    [InvoiceAmount] float  NOT NULL,
    [PremiumAmount] float  NOT NULL,
    [Portfolio] nvarchar(256)  NOT NULL,
    [CounterpartyId] int  NOT NULL
);
GO

-- Creating table 'TradeFees'
CREATE TABLE [dbo].[TradeFees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TradeId] int  NOT NULL,
    [CurrencyId] int  NOT NULL,
    [Amount] float  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Broker] nvarchar(max)  NOT NULL,
    [Type] int  NOT NULL
);
GO

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Alias] nvarchar(64)  NOT NULL
);
GO

-- Creating table 'Counterparties'
CREATE TABLE [dbo].[Counterparties] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [LegalEntity] nvarchar(max)  NOT NULL,
    [isActive] bit  NOT NULL,
    [AllocationPercent] float  NOT NULL,
    [CustomerId] int  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(32)  NOT NULL,
    [FirstName] nvarchar(64)  NOT NULL,
    [LastName] nvarchar(64)  NOT NULL,
    [Email] nvarchar(128)  NOT NULL,
    [Role] int  NOT NULL
);
GO

-- Creating table 'Deals'
CREATE TABLE [dbo].[Deals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CustomerId] int  NOT NULL,
    [Status] int  NOT NULL,
    [TradeDate] nvarchar(max)  NOT NULL,
    [CreateDate] nvarchar(max)  NOT NULL,
    [QuoteDate] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [CFTCMark] float  NOT NULL,
    [USIcode] nvarchar(256)  NULL,
    [QuoteType] int  NOT NULL
);
GO

-- Creating table 'DealOrders'
CREATE TABLE [dbo].[DealOrders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DealId] int  NOT NULL,
    [EntryDate] nvarchar(max)  NOT NULL,
    [ExecutionDate] nvarchar(max)  NOT NULL,
    [CancelDate] nvarchar(max)  NOT NULL,
    [GoodUntilDate] nvarchar(max)  NOT NULL,
    [GoodUntilTime] nvarchar(max)  NOT NULL,
    [LimitPrice] float  NOT NULL,
    [Type] int  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'Chains'
CREATE TABLE [dbo].[Chains] (
    [UnderlyingId] int  NOT NULL,
    [Expiration] datetime  NOT NULL,
    [isExchange] bit  NOT NULL,
    [isTerminal] bit  NOT NULL,
    [useForCalibration] bit  NOT NULL,
    [DefaultWidth] float  NOT NULL,
    [MinStrikeDiff] float  NOT NULL,
    [MinTick] float  NOT NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'ChainSections'
CREATE TABLE [dbo].[ChainSections] (
    [Type] int  NOT NULL,
    [ChainExpiration] datetime  NOT NULL,
    [ChainUnderlyingId] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [CalibPrice] float  NOT NULL,
    [ATMVolChange] float  NOT NULL
);
GO

-- Creating table 'SectionPoints'
CREATE TABLE [dbo].[SectionPoints] (
    [ChainSectionType] int  NOT NULL,
    [ChainSectionChainExpiration] datetime  NOT NULL,
    [ChainSectionChainUnderlyingId] int  NOT NULL,
    [ChainSectionDate] datetime  NOT NULL,
    [CallDelta] float  NOT NULL,
    [Volatility] float  NOT NULL,
    [Width] float  NOT NULL,
    [VolWidth] float  NULL,
    [OptPrice] float  NULL
);
GO

-- Creating table 'Markets_ForwardFXMarket'
CREATE TABLE [dbo].[Markets_ForwardFXMarket] (
    [QtyCurrencyId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Underlying'
CREATE TABLE [dbo].[Instruments_Underlying] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Derivative'
CREATE TABLE [dbo].[Instruments_Derivative] (
    [UnderlyingId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Markets_FuturesMarket'
CREATE TABLE [dbo].[Markets_FuturesMarket] (
    [Units] nvarchar(64)  NOT NULL,
    [CurrencyMult] int  NOT NULL,
    [UnitsPerContract] float  NOT NULL,
    [ClearFeeDefault] float  NOT NULL,
    [ExecFeeDefault] float  NOT NULL,
    [MinTick] float  NOT NULL,
    [MinOptTick] float  NOT NULL,
    [SettleType] int  NOT NULL,
    [ContractDayType] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Future'
CREATE TABLE [dbo].[Instruments_Future] (
    [FirstNotice] datetime  NOT NULL,
    [Month] int  NOT NULL,
    [Year] int  NOT NULL,
    [FuturesMarketId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Markets_InterestRateMarket'
CREATE TABLE [dbo].[Markets_InterestRateMarket] (
    [Convention] int  NOT NULL,
    [TenorType] int  NOT NULL,
    [isDefault] bit  NOT NULL,
    [isOnShore] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_InterestRate'
CREATE TABLE [dbo].[Instruments_InterestRate] (
    [Tenor] float  NOT NULL,
    [InterestRateMarketId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_ZeroCouponBond'
CREATE TABLE [dbo].[Instruments_ZeroCouponBond] (
    [InterestRateMarketId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_ForwardFXRate'
CREATE TABLE [dbo].[Instruments_ForwardFXRate] (
    [ForwardFXMarketId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_TenorFXRate'
CREATE TABLE [dbo].[Instruments_TenorFXRate] (
    [Tenor] float  NOT NULL,
    [ForwardFXMarketId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Option'
CREATE TABLE [dbo].[Instruments_Option] (
    [Strike] float  NOT NULL,
    [CallPut] int  NOT NULL,
    [ChainExpiration] datetime  NULL,
    [ChainUnderlyingId] int  NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Swap'
CREATE TABLE [dbo].[Instruments_Swap] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_IndexFuture'
CREATE TABLE [dbo].[Instruments_IndexFuture] (
    [isAverage] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_European'
CREATE TABLE [dbo].[Instruments_European] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_American'
CREATE TABLE [dbo].[Instruments_American] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Digital'
CREATE TABLE [dbo].[Instruments_Digital] (
    [Rebate] float  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Asian'
CREATE TABLE [dbo].[Instruments_Asian] (
    [AverageStart] datetime  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_DigitalAsian'
CREATE TABLE [dbo].[Instruments_DigitalAsian] (
    [Rebate] float  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'Instruments_Barrier'
CREATE TABLE [dbo].[Instruments_Barrier] (
    [Type] int  NOT NULL,
    [Frequency] int  NOT NULL,
    [MonitorDate] datetime  NOT NULL,
    [isKnocked] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'CustomerUser'
CREATE TABLE [dbo].[CustomerUser] (
    [Customers_Id] int  NOT NULL,
    [Users_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'HolidayLists'
ALTER TABLE [dbo].[HolidayLists]
ADD CONSTRAINT [PK_HolidayLists]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Date], [HolidayListId] in table 'Holidays'
ALTER TABLE [dbo].[Holidays]
ADD CONSTRAINT [PK_Holidays]
    PRIMARY KEY CLUSTERED ([Date], [HolidayListId] ASC);
GO

-- Creating primary key on [Id] in table 'Currencies'
ALTER TABLE [dbo].[Currencies]
ADD CONSTRAINT [PK_Currencies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Markets'
ALTER TABLE [dbo].[Markets]
ADD CONSTRAINT [PK_Markets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Indices'
ALTER TABLE [dbo].[Indices]
ADD CONSTRAINT [PK_Indices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Date], [IndexId], [PriceType] in table 'IndexPrices'
ALTER TABLE [dbo].[IndexPrices]
ADD CONSTRAINT [PK_IndexPrices]
    PRIMARY KEY CLUSTERED ([Date], [IndexId], [PriceType] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [PK_Instruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Date], [InstrumentId], [PriceType] in table 'InstPrices'
ALTER TABLE [dbo].[InstPrices]
ADD CONSTRAINT [PK_InstPrices]
    PRIMARY KEY CLUSTERED ([Date], [InstrumentId], [PriceType] ASC);
GO

-- Creating primary key on [Id] in table 'Exchanges'
ALTER TABLE [dbo].[Exchanges]
ADD CONSTRAINT [PK_Exchanges]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PriceSources'
ALTER TABLE [dbo].[PriceSources]
ADD CONSTRAINT [PK_PriceSources]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [InstrumentId], [PriceSourceId] in table 'InstAlias'
ALTER TABLE [dbo].[InstAlias]
ADD CONSTRAINT [PK_InstAlias]
    PRIMARY KEY CLUSTERED ([InstrumentId], [PriceSourceId] ASC);
GO

-- Creating primary key on [PriceSourceId], [FuturesMarketId] in table 'FutMarketAlias'
ALTER TABLE [dbo].[FutMarketAlias]
ADD CONSTRAINT [PK_FutMarketAlias]
    PRIMARY KEY CLUSTERED ([PriceSourceId], [FuturesMarketId] ASC);
GO

-- Creating primary key on [Id] in table 'UnderMaps'
ALTER TABLE [dbo].[UnderMaps]
ADD CONSTRAINT [PK_UnderMaps]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MapInstructions'
ALTER TABLE [dbo].[MapInstructions]
ADD CONSTRAINT [PK_MapInstructions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [PK_Trades]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TradeFees'
ALTER TABLE [dbo].[TradeFees]
ADD CONSTRAINT [PK_TradeFees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Counterparties'
ALTER TABLE [dbo].[Counterparties]
ADD CONSTRAINT [PK_Counterparties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Deals'
ALTER TABLE [dbo].[Deals]
ADD CONSTRAINT [PK_Deals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DealOrders'
ALTER TABLE [dbo].[DealOrders]
ADD CONSTRAINT [PK_DealOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Expiration], [UnderlyingId] in table 'Chains'
ALTER TABLE [dbo].[Chains]
ADD CONSTRAINT [PK_Chains]
    PRIMARY KEY CLUSTERED ([Expiration], [UnderlyingId] ASC);
GO

-- Creating primary key on [Type], [ChainExpiration], [ChainUnderlyingId], [Date] in table 'ChainSections'
ALTER TABLE [dbo].[ChainSections]
ADD CONSTRAINT [PK_ChainSections]
    PRIMARY KEY CLUSTERED ([Type], [ChainExpiration], [ChainUnderlyingId], [Date] ASC);
GO

-- Creating primary key on [ChainSectionType], [ChainSectionChainExpiration], [ChainSectionChainUnderlyingId], [ChainSectionDate], [CallDelta] in table 'SectionPoints'
ALTER TABLE [dbo].[SectionPoints]
ADD CONSTRAINT [PK_SectionPoints]
    PRIMARY KEY CLUSTERED ([ChainSectionType], [ChainSectionChainExpiration], [ChainSectionChainUnderlyingId], [ChainSectionDate], [CallDelta] ASC);
GO

-- Creating primary key on [Id] in table 'Markets_ForwardFXMarket'
ALTER TABLE [dbo].[Markets_ForwardFXMarket]
ADD CONSTRAINT [PK_Markets_ForwardFXMarket]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Underlying'
ALTER TABLE [dbo].[Instruments_Underlying]
ADD CONSTRAINT [PK_Instruments_Underlying]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Derivative'
ALTER TABLE [dbo].[Instruments_Derivative]
ADD CONSTRAINT [PK_Instruments_Derivative]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Markets_FuturesMarket'
ALTER TABLE [dbo].[Markets_FuturesMarket]
ADD CONSTRAINT [PK_Markets_FuturesMarket]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Future'
ALTER TABLE [dbo].[Instruments_Future]
ADD CONSTRAINT [PK_Instruments_Future]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Markets_InterestRateMarket'
ALTER TABLE [dbo].[Markets_InterestRateMarket]
ADD CONSTRAINT [PK_Markets_InterestRateMarket]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_InterestRate'
ALTER TABLE [dbo].[Instruments_InterestRate]
ADD CONSTRAINT [PK_Instruments_InterestRate]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_ZeroCouponBond'
ALTER TABLE [dbo].[Instruments_ZeroCouponBond]
ADD CONSTRAINT [PK_Instruments_ZeroCouponBond]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_ForwardFXRate'
ALTER TABLE [dbo].[Instruments_ForwardFXRate]
ADD CONSTRAINT [PK_Instruments_ForwardFXRate]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_TenorFXRate'
ALTER TABLE [dbo].[Instruments_TenorFXRate]
ADD CONSTRAINT [PK_Instruments_TenorFXRate]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Option'
ALTER TABLE [dbo].[Instruments_Option]
ADD CONSTRAINT [PK_Instruments_Option]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Swap'
ALTER TABLE [dbo].[Instruments_Swap]
ADD CONSTRAINT [PK_Instruments_Swap]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_IndexFuture'
ALTER TABLE [dbo].[Instruments_IndexFuture]
ADD CONSTRAINT [PK_Instruments_IndexFuture]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_European'
ALTER TABLE [dbo].[Instruments_European]
ADD CONSTRAINT [PK_Instruments_European]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_American'
ALTER TABLE [dbo].[Instruments_American]
ADD CONSTRAINT [PK_Instruments_American]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Digital'
ALTER TABLE [dbo].[Instruments_Digital]
ADD CONSTRAINT [PK_Instruments_Digital]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Asian'
ALTER TABLE [dbo].[Instruments_Asian]
ADD CONSTRAINT [PK_Instruments_Asian]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_DigitalAsian'
ALTER TABLE [dbo].[Instruments_DigitalAsian]
ADD CONSTRAINT [PK_Instruments_DigitalAsian]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Instruments_Barrier'
ALTER TABLE [dbo].[Instruments_Barrier]
ADD CONSTRAINT [PK_Instruments_Barrier]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Customers_Id], [Users_Id] in table 'CustomerUser'
ALTER TABLE [dbo].[CustomerUser]
ADD CONSTRAINT [PK_CustomerUser]
    PRIMARY KEY CLUSTERED ([Customers_Id], [Users_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [HolidayListId] in table 'Holidays'
ALTER TABLE [dbo].[Holidays]
ADD CONSTRAINT [FK_HolidayListHoliday]
    FOREIGN KEY ([HolidayListId])
    REFERENCES [dbo].[HolidayLists]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HolidayListHoliday'
CREATE INDEX [IX_FK_HolidayListHoliday]
ON [dbo].[Holidays]
    ([HolidayListId]);
GO

-- Creating foreign key on [HolidayListId] in table 'Currencies'
ALTER TABLE [dbo].[Currencies]
ADD CONSTRAINT [FK_HolidayListCurrency]
    FOREIGN KEY ([HolidayListId])
    REFERENCES [dbo].[HolidayLists]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HolidayListCurrency'
CREATE INDEX [IX_FK_HolidayListCurrency]
ON [dbo].[Currencies]
    ([HolidayListId]);
GO

-- Creating foreign key on [HolidayListId] in table 'Markets'
ALTER TABLE [dbo].[Markets]
ADD CONSTRAINT [FK_HolidayListMarket]
    FOREIGN KEY ([HolidayListId])
    REFERENCES [dbo].[HolidayLists]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HolidayListMarket'
CREATE INDEX [IX_FK_HolidayListMarket]
ON [dbo].[Markets]
    ([HolidayListId]);
GO

-- Creating foreign key on [CurrencyId] in table 'Markets'
ALTER TABLE [dbo].[Markets]
ADD CONSTRAINT [FK_CurrencyMarket]
    FOREIGN KEY ([CurrencyId])
    REFERENCES [dbo].[Currencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurrencyMarket'
CREATE INDEX [IX_FK_CurrencyMarket]
ON [dbo].[Markets]
    ([CurrencyId]);
GO

-- Creating foreign key on [HolidayListId] in table 'Indices'
ALTER TABLE [dbo].[Indices]
ADD CONSTRAINT [FK_HolidayListIndex]
    FOREIGN KEY ([HolidayListId])
    REFERENCES [dbo].[HolidayLists]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HolidayListIndex'
CREATE INDEX [IX_FK_HolidayListIndex]
ON [dbo].[Indices]
    ([HolidayListId]);
GO

-- Creating foreign key on [Market_Id] in table 'Indices'
ALTER TABLE [dbo].[Indices]
ADD CONSTRAINT [FK_MarketIndex]
    FOREIGN KEY ([Market_Id])
    REFERENCES [dbo].[Markets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MarketIndex'
CREATE INDEX [IX_FK_MarketIndex]
ON [dbo].[Indices]
    ([Market_Id]);
GO

-- Creating foreign key on [IndexId] in table 'IndexPrices'
ALTER TABLE [dbo].[IndexPrices]
ADD CONSTRAINT [FK_IndexIndexPrice]
    FOREIGN KEY ([IndexId])
    REFERENCES [dbo].[Indices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_IndexIndexPrice'
CREATE INDEX [IX_FK_IndexIndexPrice]
ON [dbo].[IndexPrices]
    ([IndexId]);
GO

-- Creating foreign key on [InstrumentId] in table 'InstPrices'
ALTER TABLE [dbo].[InstPrices]
ADD CONSTRAINT [FK_InstrumentInstPrice]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentInstPrice'
CREATE INDEX [IX_FK_InstrumentInstPrice]
ON [dbo].[InstPrices]
    ([InstrumentId]);
GO

-- Creating foreign key on [QtyCurrencyId] in table 'Markets_ForwardFXMarket'
ALTER TABLE [dbo].[Markets_ForwardFXMarket]
ADD CONSTRAINT [FK_CurrencyForwardFXMarket]
    FOREIGN KEY ([QtyCurrencyId])
    REFERENCES [dbo].[Currencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurrencyForwardFXMarket'
CREATE INDEX [IX_FK_CurrencyForwardFXMarket]
ON [dbo].[Markets_ForwardFXMarket]
    ([QtyCurrencyId]);
GO

-- Creating foreign key on [UnderlyingId] in table 'Instruments_Derivative'
ALTER TABLE [dbo].[Instruments_Derivative]
ADD CONSTRAINT [FK_UnderlyingDerivative]
    FOREIGN KEY ([UnderlyingId])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UnderlyingDerivative'
CREATE INDEX [IX_FK_UnderlyingDerivative]
ON [dbo].[Instruments_Derivative]
    ([UnderlyingId]);
GO

-- Creating foreign key on [FuturesMarketId] in table 'Instruments_Future'
ALTER TABLE [dbo].[Instruments_Future]
ADD CONSTRAINT [FK_FuturesMarketFuture]
    FOREIGN KEY ([FuturesMarketId])
    REFERENCES [dbo].[Markets_FuturesMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FuturesMarketFuture'
CREATE INDEX [IX_FK_FuturesMarketFuture]
ON [dbo].[Instruments_Future]
    ([FuturesMarketId]);
GO

-- Creating foreign key on [ExchangeId] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [FK_ExchangeInstrument]
    FOREIGN KEY ([ExchangeId])
    REFERENCES [dbo].[Exchanges]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ExchangeInstrument'
CREATE INDEX [IX_FK_ExchangeInstrument]
ON [dbo].[Instruments]
    ([ExchangeId]);
GO

-- Creating foreign key on [InstrumentId] in table 'InstAlias'
ALTER TABLE [dbo].[InstAlias]
ADD CONSTRAINT [FK_InstrumentInstAlias]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PriceSourceId] in table 'InstAlias'
ALTER TABLE [dbo].[InstAlias]
ADD CONSTRAINT [FK_PriceSourceInstAlias]
    FOREIGN KEY ([PriceSourceId])
    REFERENCES [dbo].[PriceSources]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PriceSourceInstAlias'
CREATE INDEX [IX_FK_PriceSourceInstAlias]
ON [dbo].[InstAlias]
    ([PriceSourceId]);
GO

-- Creating foreign key on [PriceSourceId] in table 'FutMarketAlias'
ALTER TABLE [dbo].[FutMarketAlias]
ADD CONSTRAINT [FK_PriceSourceFutMarketAlias]
    FOREIGN KEY ([PriceSourceId])
    REFERENCES [dbo].[PriceSources]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [FuturesMarketId] in table 'FutMarketAlias'
ALTER TABLE [dbo].[FutMarketAlias]
ADD CONSTRAINT [FK_FuturesMarketFutMarketAlias]
    FOREIGN KEY ([FuturesMarketId])
    REFERENCES [dbo].[Markets_FuturesMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FuturesMarketFutMarketAlias'
CREATE INDEX [IX_FK_FuturesMarketFutMarketAlias]
ON [dbo].[FutMarketAlias]
    ([FuturesMarketId]);
GO

-- Creating foreign key on [UnderlyingId] in table 'UnderMaps'
ALTER TABLE [dbo].[UnderMaps]
ADD CONSTRAINT [FK_UnderlyingUnderMap]
    FOREIGN KEY ([UnderlyingId])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UnderlyingUnderMap'
CREATE INDEX [IX_FK_UnderlyingUnderMap]
ON [dbo].[UnderMaps]
    ([UnderlyingId]);
GO

-- Creating foreign key on [UnderMapId] in table 'MapInstructions'
ALTER TABLE [dbo].[MapInstructions]
ADD CONSTRAINT [FK_UnderMapMapInstruction]
    FOREIGN KEY ([UnderMapId])
    REFERENCES [dbo].[UnderMaps]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UnderMapMapInstruction'
CREATE INDEX [IX_FK_UnderMapMapInstruction]
ON [dbo].[MapInstructions]
    ([UnderMapId]);
GO

-- Creating foreign key on [SourceUnderlyingId] in table 'MapInstructions'
ALTER TABLE [dbo].[MapInstructions]
ADD CONSTRAINT [FK_UnderlyingMapInstruction]
    FOREIGN KEY ([SourceUnderlyingId])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UnderlyingMapInstruction'
CREATE INDEX [IX_FK_UnderlyingMapInstruction]
ON [dbo].[MapInstructions]
    ([SourceUnderlyingId]);
GO

-- Creating foreign key on [InterestRateMarketId] in table 'Instruments_InterestRate'
ALTER TABLE [dbo].[Instruments_InterestRate]
ADD CONSTRAINT [FK_InterestRateMarketInterestRate]
    FOREIGN KEY ([InterestRateMarketId])
    REFERENCES [dbo].[Markets_InterestRateMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InterestRateMarketInterestRate'
CREATE INDEX [IX_FK_InterestRateMarketInterestRate]
ON [dbo].[Instruments_InterestRate]
    ([InterestRateMarketId]);
GO

-- Creating foreign key on [InterestRateMarketId] in table 'Instruments_ZeroCouponBond'
ALTER TABLE [dbo].[Instruments_ZeroCouponBond]
ADD CONSTRAINT [FK_InterestRateMarketZeroCouponBond]
    FOREIGN KEY ([InterestRateMarketId])
    REFERENCES [dbo].[Markets_InterestRateMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InterestRateMarketZeroCouponBond'
CREATE INDEX [IX_FK_InterestRateMarketZeroCouponBond]
ON [dbo].[Instruments_ZeroCouponBond]
    ([InterestRateMarketId]);
GO

-- Creating foreign key on [ForwardFXMarketId] in table 'Instruments_ForwardFXRate'
ALTER TABLE [dbo].[Instruments_ForwardFXRate]
ADD CONSTRAINT [FK_ForwardFXMarketForwardFXRate]
    FOREIGN KEY ([ForwardFXMarketId])
    REFERENCES [dbo].[Markets_ForwardFXMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ForwardFXMarketForwardFXRate'
CREATE INDEX [IX_FK_ForwardFXMarketForwardFXRate]
ON [dbo].[Instruments_ForwardFXRate]
    ([ForwardFXMarketId]);
GO

-- Creating foreign key on [ForwardFXMarketId] in table 'Instruments_TenorFXRate'
ALTER TABLE [dbo].[Instruments_TenorFXRate]
ADD CONSTRAINT [FK_ForwardFXMarketTenorFXRate]
    FOREIGN KEY ([ForwardFXMarketId])
    REFERENCES [dbo].[Markets_ForwardFXMarket]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ForwardFXMarketTenorFXRate'
CREATE INDEX [IX_FK_ForwardFXMarketTenorFXRate]
ON [dbo].[Instruments_TenorFXRate]
    ([ForwardFXMarketId]);
GO

-- Creating foreign key on [InstrumentId] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [FK_InstrumentTrade]
    FOREIGN KEY ([InstrumentId])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InstrumentTrade'
CREATE INDEX [IX_FK_InstrumentTrade]
ON [dbo].[Trades]
    ([InstrumentId]);
GO

-- Creating foreign key on [TradeId] in table 'TradeFees'
ALTER TABLE [dbo].[TradeFees]
ADD CONSTRAINT [FK_TradeTradeFee]
    FOREIGN KEY ([TradeId])
    REFERENCES [dbo].[Trades]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TradeTradeFee'
CREATE INDEX [IX_FK_TradeTradeFee]
ON [dbo].[TradeFees]
    ([TradeId]);
GO

-- Creating foreign key on [CurrencyId] in table 'TradeFees'
ALTER TABLE [dbo].[TradeFees]
ADD CONSTRAINT [FK_CurrencyTradeFee]
    FOREIGN KEY ([CurrencyId])
    REFERENCES [dbo].[Currencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurrencyTradeFee'
CREATE INDEX [IX_FK_CurrencyTradeFee]
ON [dbo].[TradeFees]
    ([CurrencyId]);
GO

-- Creating foreign key on [CustomerId] in table 'Counterparties'
ALTER TABLE [dbo].[Counterparties]
ADD CONSTRAINT [FK_CustomerCounterparty]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerCounterparty'
CREATE INDEX [IX_FK_CustomerCounterparty]
ON [dbo].[Counterparties]
    ([CustomerId]);
GO

-- Creating foreign key on [CounterpartyId] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [FK_CounterpartyTrade]
    FOREIGN KEY ([CounterpartyId])
    REFERENCES [dbo].[Counterparties]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CounterpartyTrade'
CREATE INDEX [IX_FK_CounterpartyTrade]
ON [dbo].[Trades]
    ([CounterpartyId]);
GO

-- Creating foreign key on [Customers_Id] in table 'CustomerUser'
ALTER TABLE [dbo].[CustomerUser]
ADD CONSTRAINT [FK_CustomerUser_Customer]
    FOREIGN KEY ([Customers_Id])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Users_Id] in table 'CustomerUser'
ALTER TABLE [dbo].[CustomerUser]
ADD CONSTRAINT [FK_CustomerUser_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerUser_User'
CREATE INDEX [IX_FK_CustomerUser_User]
ON [dbo].[CustomerUser]
    ([Users_Id]);
GO

-- Creating foreign key on [CustomerId] in table 'Deals'
ALTER TABLE [dbo].[Deals]
ADD CONSTRAINT [FK_CustomerDeal]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerDeal'
CREATE INDEX [IX_FK_CustomerDeal]
ON [dbo].[Deals]
    ([CustomerId]);
GO

-- Creating foreign key on [DealId] in table 'DealOrders'
ALTER TABLE [dbo].[DealOrders]
ADD CONSTRAINT [FK_DealDealOrder]
    FOREIGN KEY ([DealId])
    REFERENCES [dbo].[Deals]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DealDealOrder'
CREATE INDEX [IX_FK_DealDealOrder]
ON [dbo].[DealOrders]
    ([DealId]);
GO

-- Creating foreign key on [UnderlyingId] in table 'Chains'
ALTER TABLE [dbo].[Chains]
ADD CONSTRAINT [FK_UnderlyingChain]
    FOREIGN KEY ([UnderlyingId])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UnderlyingChain'
CREATE INDEX [IX_FK_UnderlyingChain]
ON [dbo].[Chains]
    ([UnderlyingId]);
GO

-- Creating foreign key on [ChainExpiration], [ChainUnderlyingId] in table 'ChainSections'
ALTER TABLE [dbo].[ChainSections]
ADD CONSTRAINT [FK_ChainChainSection]
    FOREIGN KEY ([ChainExpiration], [ChainUnderlyingId])
    REFERENCES [dbo].[Chains]
        ([Expiration], [UnderlyingId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChainChainSection'
CREATE INDEX [IX_FK_ChainChainSection]
ON [dbo].[ChainSections]
    ([ChainExpiration], [ChainUnderlyingId]);
GO

-- Creating foreign key on [ChainSectionType], [ChainSectionChainExpiration], [ChainSectionChainUnderlyingId], [ChainSectionDate] in table 'SectionPoints'
ALTER TABLE [dbo].[SectionPoints]
ADD CONSTRAINT [FK_ChainSectionSectionPoint]
    FOREIGN KEY ([ChainSectionType], [ChainSectionChainExpiration], [ChainSectionChainUnderlyingId], [ChainSectionDate])
    REFERENCES [dbo].[ChainSections]
        ([Type], [ChainExpiration], [ChainUnderlyingId], [Date])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ChainExpiration], [ChainUnderlyingId] in table 'Instruments_Option'
ALTER TABLE [dbo].[Instruments_Option]
ADD CONSTRAINT [FK_ChainOption]
    FOREIGN KEY ([ChainExpiration], [ChainUnderlyingId])
    REFERENCES [dbo].[Chains]
        ([Expiration], [UnderlyingId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChainOption'
CREATE INDEX [IX_FK_ChainOption]
ON [dbo].[Instruments_Option]
    ([ChainExpiration], [ChainUnderlyingId]);
GO

-- Creating foreign key on [Id] in table 'Markets_ForwardFXMarket'
ALTER TABLE [dbo].[Markets_ForwardFXMarket]
ADD CONSTRAINT [FK_ForwardFXMarket_inherits_Market]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Markets]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Underlying'
ALTER TABLE [dbo].[Instruments_Underlying]
ADD CONSTRAINT [FK_Underlying_inherits_Instrument]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Derivative'
ALTER TABLE [dbo].[Instruments_Derivative]
ADD CONSTRAINT [FK_Derivative_inherits_Instrument]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Markets_FuturesMarket'
ALTER TABLE [dbo].[Markets_FuturesMarket]
ADD CONSTRAINT [FK_FuturesMarket_inherits_Market]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Markets]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Future'
ALTER TABLE [dbo].[Instruments_Future]
ADD CONSTRAINT [FK_Future_inherits_Underlying]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Markets_InterestRateMarket'
ALTER TABLE [dbo].[Markets_InterestRateMarket]
ADD CONSTRAINT [FK_InterestRateMarket_inherits_Market]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Markets]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_InterestRate'
ALTER TABLE [dbo].[Instruments_InterestRate]
ADD CONSTRAINT [FK_InterestRate_inherits_Underlying]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_ZeroCouponBond'
ALTER TABLE [dbo].[Instruments_ZeroCouponBond]
ADD CONSTRAINT [FK_ZeroCouponBond_inherits_Underlying]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_ForwardFXRate'
ALTER TABLE [dbo].[Instruments_ForwardFXRate]
ADD CONSTRAINT [FK_ForwardFXRate_inherits_Underlying]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_TenorFXRate'
ALTER TABLE [dbo].[Instruments_TenorFXRate]
ADD CONSTRAINT [FK_TenorFXRate_inherits_Underlying]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Underlying]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Option'
ALTER TABLE [dbo].[Instruments_Option]
ADD CONSTRAINT [FK_Option_inherits_Derivative]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Derivative]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Swap'
ALTER TABLE [dbo].[Instruments_Swap]
ADD CONSTRAINT [FK_Swap_inherits_Derivative]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Derivative]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_IndexFuture'
ALTER TABLE [dbo].[Instruments_IndexFuture]
ADD CONSTRAINT [FK_IndexFuture_inherits_Future]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Future]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_European'
ALTER TABLE [dbo].[Instruments_European]
ADD CONSTRAINT [FK_European_inherits_Option]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Option]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_American'
ALTER TABLE [dbo].[Instruments_American]
ADD CONSTRAINT [FK_American_inherits_Option]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Option]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Digital'
ALTER TABLE [dbo].[Instruments_Digital]
ADD CONSTRAINT [FK_Digital_inherits_Option]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Option]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Asian'
ALTER TABLE [dbo].[Instruments_Asian]
ADD CONSTRAINT [FK_Asian_inherits_Option]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Option]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_DigitalAsian'
ALTER TABLE [dbo].[Instruments_DigitalAsian]
ADD CONSTRAINT [FK_DigitalAsian_inherits_Asian]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Asian]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'Instruments_Barrier'
ALTER TABLE [dbo].[Instruments_Barrier]
ADD CONSTRAINT [FK_Barrier_inherits_Option]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Instruments_Option]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
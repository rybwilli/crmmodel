//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class FuturesMarket : Market
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FuturesMarket()
        {
            this.Futures = new HashSet<Future>();
            this.FutMarketAlias = new HashSet<FutMarketAlias>();
        }
    
        public string Units { get; set; }
        public int CurrencyMult { get; set; }
        public double UnitsPerContract { get; set; }
        public double ClearFeeDefault { get; set; }
        public double ExecFeeDefault { get; set; }
        public double MinTick { get; set; }
        public double MinOptTick { get; set; }
        public Settlement SettleType { get; set; }
        public ContractDay ContractDayType { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Future> Futures { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FutMarketAlias> FutMarketAlias { get; set; }
    }
}

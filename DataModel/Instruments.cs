﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public abstract partial class Instrument
    {
        public override string ToString()
        {
            return this.Symbol;
        }
        public abstract Market getMarket();
        public abstract double quantityMulitplier();
    }
    public abstract partial class Underlying
    {
        private UnderMap primaryMap;
        public UnderMap PrimaryMap { get { return primaryMap; } set { primaryMap = value; } }
        public void initMap()
        {
            // we will build in roles/groups later
            primaryMap = this.UnderMaps.Where(x => x.isActive).FirstOrDefault();
        }
    }
    public abstract partial class Derivative
    {
        public override Market getMarket()
        {
            return this.Underlying.getMarket();
        }
        public override double quantityMulitplier()
        {
            return this.Underlying.quantityMulitplier();
        }
    }
    public partial class Future
    {
        public override Market getMarket()
        {
            return this.FuturesMarket;
        }
        public override double quantityMulitplier()
        {
            switch(this.FuturesMarket.ContractDayType)
            {
                case ContractDay.calendar:
                    DateTime dt = new DateTime(this.Year, this.Month, 1);
                    return this.FuturesMarket.UnitsPerContract / this.FuturesMarket.CurrencyMult * (dt.AddMonths(1) - dt).Days;
                default:
                    return this.FuturesMarket.UnitsPerContract / this.FuturesMarket.CurrencyMult;
            }
        }
    }
    public partial class ForwardFXRate
    {
        public override Market getMarket()
        {
            return this.ForwardFXMarket;
        }
    }
    public partial class TenorFXRate
    {
        public override Market getMarket()
        {
            return this.ForwardFXMarket;
        }
    }
    public partial class InterestRate
    {
        public override Market getMarket()
        {
            return this.InterestRateMarket;
        }
    }
    public partial class ZeroCouponBond
    {
        public override Market getMarket()
        {
            return this.InterestRateMarket;
        }
    }
}

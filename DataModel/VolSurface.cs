﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public partial class SectionPoint
    {
        public double delta() { return CallDelta > 0.5 ? CallDelta-1 : CallDelta; }
    }
    public partial class ChainSection
    {
        public UnderRecord under;
    }
    public class VolSurface
    {
        public List<ChainSection> sections = new List<ChainSection>();
        public List<ChainSection> fwdSections = new List<ChainSection>();
        public MarketState state;
        public FuturesMarket market;
        public VolSurface(string mktsymbol,MarketState _ms)
        {
            state = _ms;
            market = state.dmc.Markets.OfType<FuturesMarket>().SingleOrDefault(x => x.Symbol == mktsymbol.ToUpper());
            foreach (var f in market.Futures.Where(x => x.Expiration>=state.valueDate))
            {
                var upx = state.getUnderRecord(f.Symbol);
                var secs = state.dmc.ChainSections.Include("SectionPoints").Where(x => x.ChainUnderlyingId == f.Id && x.Type == SectionType.live).GroupBy(x => x.ChainExpiration).Select(x => x.OrderBy(y => y.Date).LastOrDefault()).ToList();
                secs.ForEach(x => x.under = upx);
                sections.AddRange(secs);
            }
        }
        public void buildSurface()
        {
            try
            {

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        //splines vols and width by delta for terminal chains
        public void SplineTerminalSurface()
        {
            var tsecs = sections.Where(x => x.Chain.isTerminal && !x.Chain.useForCalibration);
            if (tsecs.Count()>0)
            {
                foreach(var t in tsecs)
                {
                    t.SectionPoints.Clear();
                    
                }
            }
        }
    }
}
